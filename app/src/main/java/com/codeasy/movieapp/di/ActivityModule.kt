package com.codeasy.movieapp.di

import com.codeasy.movieapp.ui.movie.detail.MovieDetailActivity
import com.codeasy.movieapp.ui.movie.list.MovieListActivity
import com.codeasy.movieapp.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMovieListActivity(): MovieListActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMovieDetailActivity(): MovieDetailActivity
}

