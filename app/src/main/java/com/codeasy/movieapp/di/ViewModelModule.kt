package com.codeasy.movieapp.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.codeasy.movieapp.ui.movie.list.MovieListActivityViewModel
import com.codeasy.movieapp.factory.AppViewModelFactory
import com.codeasy.movieapp.ui.movie.MovieFavoriteViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    /**
     * Here we're providing entries to a multibound map, providing the value (ViewModel) and the key (ViewModelKey)
     */

    @Binds
    @IntoMap
    @ViewModelKey(MovieListActivityViewModel::class)
    internal abstract fun bindMovieListActivityViewModel(movieListActivityViewModel: MovieListActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieFavoriteViewModel::class)
    internal abstract fun bindMovieFavoriteViewModel(movieFavoriteViewModel: MovieFavoriteViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory
}