package com.codeasy.movieapp.di

import com.codeasy.movieapp.ui.movie.list.fragments.MovieListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    /**
     * Here we attach our Fragments to Dagger Graph
     */

    @ContributesAndroidInjector
    internal abstract fun contributeMovieListFragment(): MovieListFragment
}