package com.codeasy.movieapp.di

import android.app.Application
import com.codeasy.movieapp.MyApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class),
    (ActivityModule::class),
    (FragmentModule::class),
    (ViewModelModule::class),
    (AppModule::class),
    (NetModule::class)])
interface AppComponent {

    // Create an instance of the Component
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        @BindsInstance
        fun baseUrl(url : String) : Builder
        fun build(): AppComponent
    }

    fun inject(instance: MyApplication)
}