package com.codeasy.movieapp.utils

import java.util.*

object DateUtil {

    fun getCurrentYear() : Int {
        return Calendar.getInstance().get(Calendar.YEAR)
    }
}