package com.codeasy.movieapp.data.models

data class MovieQuery(
    var page: Int = 1,
    var year: Int = 2019,
    var title: String? = null,
    var sortBy: String = "popularity.desc"
)