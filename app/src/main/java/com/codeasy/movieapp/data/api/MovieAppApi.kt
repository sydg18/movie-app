package com.codeasy.movieapp.data.api

import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import com.codeasy.movieapp.data.models.MovieData
import com.codeasy.movieapp.data.models.MovieDetailData
import com.codeasy.movieapp.utils.DateUtil
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieAppApi {

    @GET("discover/movie/")
    fun getMovies(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("year") year: Int = DateUtil.getCurrentYear(),
        @Nullable @Query("sort_by") sortBy: String = "popularity.desc"
    ): LiveData<ApiResponse<MovieData>>

    @GET("search/movie/")
    fun getMoviesByTitle(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("query") sortBy: String
    ): LiveData<ApiResponse<MovieData>>

    @GET("movie/")
    fun getMovie(
        @Query("api_key") apiKey: String,
        @Nullable @Query("id") id: Int
    ): LiveData<ApiResponse<MovieDetailData>>
}