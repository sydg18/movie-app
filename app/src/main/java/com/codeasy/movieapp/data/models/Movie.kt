package com.codeasy.movieapp.data.models

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Entity(primaryKeys = ["id"])
@Parcelize
data class Movie(
    val id: Int,
    val title: String,
    val vote_count: Int,
    val vote_average: Double,
    val popularity: Double,
    val overview: String,
    val poster_path: String?,
    val release_date: String,
    var favorite: Boolean = false
) : Parcelable