package com.codeasy.movieapp.data.models

data class MovieDetailData(var movie: Movie)