package com.codeasy.movieapp.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.codeasy.movieapp.data.models.Movie

@Database(entities = [(Movie::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao
}