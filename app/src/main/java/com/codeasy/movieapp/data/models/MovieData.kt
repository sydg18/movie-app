package com.codeasy.movieapp.data.models

data class MovieData(val page: Int, val total_results: Int, val total_pages: Int,
                     var results: MutableList<Movie>)