package com.codeasy.movieapp.data.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.codeasy.movieapp.data.models.Movie

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: Movie)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movies: List<Movie>)

    @Update
    fun update(movies: List<Movie>)

    @Update
    fun update(movie: Movie)

    @Query("SELECT * FROM Movie WHERE Movie.id = :id")
    fun getById( id : Int) : LiveData<Movie>

    @Query("SELECT * FROM Movie ORDER BY popularity DESC LIMIT :limit OFFSET :offset")
    fun get(limit: Int, offset: Int): LiveData<List<Movie>>

    @Query("SELECT * FROM Movie ORDER BY popularity DESC")
    fun get(): LiveData<List<Movie>>


    @Query("SELECT * FROM Movie WHERE favorite = 1")
    fun getFavorites(): LiveData<List<Movie>>

    @Query("SELECT * FROM Movie WHERE favorite = 1 AND title like :title")
    fun getFavorites(title: String): LiveData<List<Movie>>

    @Query("SELECT * FROM Movie WHERE title like :title ORDER BY popularity DESC LIMIT :limit OFFSET :offset")
    fun getByTitle(title: String, limit: Int, offset: Int) : LiveData<List<Movie>>

    @Query("DELETE FROM Movie WHERE id = :id")
    fun delete(id: Int)

    @Query("DELETE FROM Movie")
    fun deleteAll()

}