package com.codeasy.movieapp.data.api

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}