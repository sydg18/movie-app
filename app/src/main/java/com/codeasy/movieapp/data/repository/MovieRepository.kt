package com.codeasy.movieapp.data.repository

import androidx.lifecycle.LiveData
import com.codeasy.movieapp.BuildConfig
import com.codeasy.movieapp.data.api.ApiResponse
import com.codeasy.movieapp.data.api.MovieAppApi
import com.codeasy.movieapp.data.api.Resource
import com.codeasy.movieapp.data.models.*
import com.codeasy.movieapp.data.room.MovieDao
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepository @Inject
constructor(val movieDao: MovieDao, val movieAppApi: MovieAppApi) {

    private val defaultLimit = 20

    var totalPage = -1

    fun getMovies(movieQuery: MovieQuery): LiveData<Resource<List<Movie>>> {

        return object : NetworkBoundResource<List<Movie>, MovieData>() {

            override fun saveFetchData(item: MovieData) {

                val movies = item.results
                totalPage = item.total_pages

                movieDao.insert(movies)
            }

            override fun shouldFetch(data: List<Movie>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun loadFromDb(): LiveData<List<Movie>> {
                val offset: Int = (movieQuery.page - 1) * defaultLimit

                var title = movieQuery.title
                if (title.isNullOrBlank()) {
                    return movieDao.get(defaultLimit, offset)
                }

                title = "%$title%"
                return movieDao.getByTitle(title, defaultLimit, offset)
            }

            override fun fetchService(): LiveData<ApiResponse<MovieData>> {

                val title = movieQuery.title
                if (title.isNullOrBlank()) {
                    return movieAppApi.getMovies(
                        BuildConfig.MOVIE_API_KEY,
                        movieQuery.page,
                        movieQuery.year,
                        movieQuery.sortBy
                    )
                }
                return movieAppApi.getMoviesByTitle(
                    BuildConfig.MOVIE_API_KEY,
                    movieQuery.page,
                    title
                )
            }

            override fun onFetchFailed() {

            }

        }.asLiveData
    }

    fun getFavorites(movieQuery: MovieQuery): LiveData<Resource<List<Movie>>> {

        return object : NetworkBoundResource<List<Movie>, MovieData>() {

            override fun saveFetchData(item: MovieData) {
            }

            override fun shouldFetch(data: List<Movie>?): Boolean {
                return false
            }

            override fun loadFromDb(): LiveData<List<Movie>> {
                var title = movieQuery.title
                if (title.isNullOrBlank()) {
                    return movieDao.getFavorites()
                }

                title = "%$title%"
                return movieDao.getFavorites(title)
            }

            override fun fetchService(): LiveData<ApiResponse<MovieData>> {
                return movieAppApi.getMovies(BuildConfig.MOVIE_API_KEY, 1, 1, "")
            }

            override fun onFetchFailed() {

            }

        }.asLiveData
    }

    fun update(movie: Movie): LiveData<Movie> {

        movieDao.update(movie)
        return movieDao.getById(movie.id)
    }
}
