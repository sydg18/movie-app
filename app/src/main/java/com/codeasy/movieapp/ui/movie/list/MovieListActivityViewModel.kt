package com.codeasy.movieapp.ui.movie.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.codeasy.movieapp.data.api.Resource
import com.codeasy.movieapp.data.models.Movie
import com.codeasy.movieapp.data.models.MovieQuery
import com.codeasy.movieapp.data.repository.MovieRepository
import com.codeasy.movieapp.ui.movie.list.fragments.MovieListAdapter
import javax.inject.Inject

class MovieListActivityViewModel @Inject
constructor(private val movieRepository: MovieRepository) : ViewModel() {

    var moviesLiveData: LiveData<Resource<List<Movie>>> = MutableLiveData()

    lateinit var adapter: MovieListAdapter

    var firstTime = false

    var favorite = false

    var pageCounter = 1

    private val movieQuery: MutableLiveData<MovieQuery> = MutableLiveData()

    init {
        moviesLiveData = Transformations.switchMap(movieQuery) {
            if (favorite) {
                movieRepository.getFavorites(movieQuery.value!!)
            } else {
                movieRepository.getMovies(movieQuery.value!!)
            }
        }
    }

    fun postPage(page: Int) {
        val newQuery = MovieQuery(page)
        if(movieQuery.value != null){
            newQuery.sortBy = movieQuery.value!!.sortBy
            newQuery.title = movieQuery.value!!.title
            newQuery.year = movieQuery.value!!.year
        }

        movieQuery.postValue(newQuery)
    }

    fun searchByTitle(query: String) {

        val newQuery = MovieQuery()
        newQuery.title = query

        movieQuery.postValue(newQuery)
    }

}