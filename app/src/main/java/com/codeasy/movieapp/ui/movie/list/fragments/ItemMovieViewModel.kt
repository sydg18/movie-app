package com.codeasy.movieapp.ui.movie.list.fragments

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.codeasy.movieapp.BuildConfig
import com.codeasy.movieapp.data.models.Movie

class ItemMovieViewModel(private val movie: Movie) : BaseObservable() {

    @Bindable
    fun getTitle(): String = movie.title

    @Bindable
    fun getRating(): String = "" + movie.vote_average

    @Bindable
    fun getVotes(): String = "" + movie.vote_count

    @Bindable
    fun getPopularity(): String = "" + movie.popularity

    @Bindable
    fun getOverview(): String = movie.overview

    @Bindable
    fun getReleaseDate(): String = movie.release_date

    fun posterPath(): String = BuildConfig.MOVIE_API_URL_IMG + movie.poster_path

    @Bindable
    fun isFavorite(): Boolean = movie.favorite
}