package com.codeasy.movieapp.ui.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.codeasy.movieapp.data.models.Movie
import com.codeasy.movieapp.data.repository.MovieRepository
import javax.inject.Inject

class MovieFavoriteViewModel @Inject
constructor(private val movieRepository: MovieRepository) : ViewModel() {

    companion object {
        var updaterMovieCounter: MutableLiveData<Int> = MutableLiveData()
        lateinit var movie : Movie
    }

    var movieFavoriteLiveData: LiveData<Movie> = MutableLiveData()


    init {
        movieFavoriteLiveData = Transformations.switchMap(updaterMovieCounter) {
            movieRepository.update(movie)
        }
    }


    fun changeFavorite(m: Movie, favorite: Boolean) {
        m.favorite = favorite
        movie = m

        if (updaterMovieCounter.value == null) {
            updaterMovieCounter.value = 1
        } else {
            updaterMovieCounter.value = updaterMovieCounter.value!! + 1
        }

    }
}