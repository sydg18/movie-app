package com.codeasy.movieapp.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.codeasy.movieapp.R
import com.codeasy.movieapp.ui.movie.list.MovieListActivity


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            val mainIntent = Intent(this, MovieListActivity::class.java)
            startActivity(mainIntent)
            finish()
        }, 3000)

    }
}