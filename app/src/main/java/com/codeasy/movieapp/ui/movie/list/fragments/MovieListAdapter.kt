package com.codeasy.movieapp.ui.movie.list.fragments

import android.view.View
import com.codeasy.movieapp.R
import com.codeasy.movieapp.base.BaseAdapter
import com.codeasy.movieapp.base.BaseViewHolder
import com.codeasy.movieapp.data.models.Movie

class MovieListAdapter(private val delegate: MovieViewHolder.Delegate) : BaseAdapter() {

    init {
        addItems(ArrayList<Movie>())
    }

    fun updateItem(movie: Movie) {
        for ((pos, m) in items.withIndex()) {
            if (m !is Movie ) {
                continue
            }
            if(m.id != movie.id){
                continue
            }
            if (m.favorite != movie.favorite) {
                m.favorite = movie.favorite
                notifyItemChanged(pos)
            }
            return
        }

    }
    fun insertItem(movie: Movie) {
        for ((pos, m) in items.withIndex()) {
            if (m !is Movie ) {
                continue
            }
            if(m.id != movie.id){
                continue
            }
            return
        }
        addItem(movie)
    }
    fun deleteItem(movie: Movie) {
        for ((pos, m) in items.withIndex()) {
            if (m !is Movie ) {
                continue
            }
            if(m.id != movie.id){
                continue
            }
            items.remove(m)
            notifyItemRemoved(pos)
            return
        }
    }

    fun updateList(movies: List<Movie>) {

        addItems(movies)
        notifyItemInserted(items.size)
    }

    override fun viewHolder(layout: Int, view: View): BaseViewHolder {
        return MovieViewHolder(view, delegate)
    }

    override fun layout(item: Any?): Int {
        return if (item == null) R.layout.item_loading else R.layout.item_movie
    }


}