package com.codeasy.movieapp.ui.movie.list.fragments

import androidx.databinding.DataBindingUtil
import android.view.View
import com.codeasy.movieapp.base.BaseViewHolder
import com.codeasy.movieapp.data.models.Movie
import com.codeasy.movieapp.databinding.ItemMovieBinding

class MovieViewHolder(view: View, private val delegate: Delegate) : BaseViewHolder(view) {

    //here we define actions that we handle
    interface Delegate {
        fun onItemClick(movie: Movie, view: View)

        fun onFavoriteClick(movie: Movie, isChecked: Boolean)
    }

    private lateinit var movie: Movie

    private val binding by lazy { DataBindingUtil.bind<ItemMovieBinding>(view) }


    init {
        binding?.favorite?.setOnCheckedChangeListener { _, isChecked ->
            if(movie.favorite != isChecked) {
                delegate.onFavoriteClick(movie, isChecked)
            }
        }
    }

    override fun bindData(data: Any?) {
        if (data is Movie) {
            movie = data
            drawUI()
        }
    }

    private fun drawUI() {
        binding.apply {
            binding?.vModel = ItemMovieViewModel(movie)
            binding?.executePendingBindings()

        }


    }

    override fun onClick(view: View?) {
        delegate.onItemClick(movie, itemView)
    }

    override fun onLongClick(view: View?): Boolean {
        return false
    }


}