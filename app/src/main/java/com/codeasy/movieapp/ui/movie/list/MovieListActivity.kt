package com.codeasy.movieapp.ui.movie.list

import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.appcompat.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.codeasy.movieapp.R
import com.codeasy.movieapp.databinding.ActivityMovieListBinding
import com.codeasy.movieapp.ui.movie.list.fragments.MovieListFragment
import java.lang.Exception
import java.util.*
import kotlin.concurrent.schedule

class MovieListActivity : AppCompatActivity() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMovieListBinding>(
            this,
            R.layout.activity_movie_list
        )
    }

    private lateinit var pagerAdapter: MovieListActivityPageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUI()
    }

    private fun initUI() {

        pagerAdapter = MovieListActivityPageAdapter(supportFragmentManager)
        binding.viewPager.adapter = pagerAdapter

        binding.toolbar.title = "Movies"
        binding.navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_movies -> {
                    binding.toolbar.title = "Movies"
                    binding.viewPager.currentItem = 0
                }
                R.id.navigation_favorites -> {
                    binding.toolbar.title = "Favorites"
                    binding.viewPager.currentItem = 1
                }
            }
            return@setOnNavigationItemSelectedListener true
        }

        binding.search.setIconifiedByDefault(false)

        binding.search.queryHint = "Search by title"

        binding.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            var timer = Timer()

            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                timer.cancel()
                val sleep = when (newText.length) {
                    1 -> 1000L
                    2, 3 -> 700L
                    4, 5 -> 500L
                    else -> 300L
                }
                timer = Timer()
                timer.schedule(sleep) {
                    search(newText)
                }
                return true
            }

        })
    }

    @MainThread
    private fun search(query: String) {

        runOnUiThread {
            Toast.makeText(this, "Searching $query", Toast.LENGTH_LONG).show()
            pagerAdapter.notifySearch(query)
        }
    }

    private class MovieListActivityPageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        lateinit var movieListFragment: MovieListFragment
        lateinit var favoriteMovieListFragment: MovieListFragment

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> {
                    movieListFragment = MovieListFragment.newInstance()
                    return movieListFragment
                }
                1 -> {
                    favoriteMovieListFragment = MovieListFragment.newInstance(true)
                    return favoriteMovieListFragment
                }
            }
            return null
        }

        fun notifySearch(query: String) {
            try {
                favoriteMovieListFragment.notifySearch(query)
                movieListFragment.notifySearch(query)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun getCount(): Int {
            return 2
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        return true
    }
}