package com.codeasy.movieapp.ui.movie.list.fragments

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codeasy.movieapp.R
import com.codeasy.movieapp.data.api.Resource
import com.codeasy.movieapp.data.api.Status
import com.codeasy.movieapp.data.models.Movie
import com.codeasy.movieapp.databinding.FragmentMovieListBinding
import com.codeasy.movieapp.factory.AppViewModelFactory
import com.codeasy.movieapp.ui.movie.MovieFavoriteViewModel
import com.codeasy.movieapp.ui.movie.detail.MovieDetailActivity
import com.codeasy.movieapp.ui.movie.list.MovieListActivityViewModel
import com.codeasy.movieapp.utils.InfiniteScrollListener
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.item_movie.view.*
import javax.inject.Inject

class MovieListFragment : Fragment(), MovieViewHolder.Delegate {

    @Inject
    lateinit var viewModelFactory: AppViewModelFactory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MovieListActivityViewModel::class.java)
    }

    private val movieFavoriteViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MovieFavoriteViewModel::class.java)
    }

    lateinit var binding: FragmentMovieListBinding


    companion object {
        private const val FAVORITE = "favorite"

        fun newInstance(favorite: Boolean = false): MovieListFragment {
            val args = Bundle()
            val fragment = MovieListFragment()
            fragment.arguments = args
            args.putBoolean(FAVORITE, favorite)
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_list, container, false)

        viewModel.favorite = arguments?.getBoolean(FAVORITE)!!
        if (savedInstanceState == null) {
            viewModel.firstTime = true
        }

        initView()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeViewModel()
    }

    private fun initView() {
        val spanCount = resources.getInteger(R.integer.movie_per_row)
        val linearLayout = androidx.recyclerview.widget.GridLayoutManager(context, spanCount)
        binding.rvMovies.layoutManager = linearLayout
        viewModel.adapter = MovieListAdapter(this)
        binding.rvMovies.adapter = viewModel.adapter
        binding.rvMovies.addOnScrollListener(InfiniteScrollListener({
            viewModel.pageCounter += 1
            loadMore(viewModel.pageCounter)
        }, linearLayout))
    }

    private fun loadMore(page: Int) {
        viewModel.postPage(page)
    }

    private fun observeViewModel() {
        viewModel.moviesLiveData.observe(this, Observer { it?.let { processResponse(it) } })

        movieFavoriteViewModel.movieFavoriteLiveData.observe(this, Observer { it?.let { processFavoriteChange(it) } })
        loadMore(viewModel.pageCounter)
    }

    private fun processResponse(response: Resource<List<Movie>>) {
        when (response.status) {
            Status.LOADING -> renderLoadingState()

            Status.SUCCESS -> renderDataState(response.data!!)

            Status.ERROR -> renderErrorState(response.error!!)
        }
    }

    private fun renderLoadingState() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun processFavoriteChange(movie: Movie) {

        if (viewModel.favorite) {
            if (movie.favorite) {
                viewModel.adapter.insertItem(movie)
            } else {
                viewModel.adapter.deleteItem(movie)
            }
        } else {
            viewModel.adapter.updateItem(movie)
        }
    }

    private fun renderDataState(items: List<Movie>) {

        binding.progressBar.visibility = View.GONE

        if (viewModel.favorite) {
            if (viewModel.firstTime) {
                viewModel.adapter.updateList(items)
            }
        } else {
            viewModel.adapter.updateList(items)
        }

        if (viewModel.firstTime) {
            binding.rvMovies.scheduleLayoutAnimation()
            viewModel.firstTime = false
        }
    }

    private fun renderErrorState(throwable: Throwable) {

        binding.progressBar.visibility = View.GONE
        // TODO: Show error
    }

    override fun onFavoriteClick(movie: Movie, isChecked: Boolean) {
        movieFavoriteViewModel.changeFavorite(movie, isChecked)
    }

    override fun onItemClick(movie: Movie, view: View) {

        val img = Pair.create(view.image as View, resources.getString(R.string.transition_movie_image))

        val name = Pair.create(view.title as View, resources.getString(R.string.transition_movie_title))

        val options = ActivityOptions.makeSceneTransitionAnimation(activity, img, name)

        val intent = Intent(context, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.intent_movie, movie as Parcelable)
        startActivity(intent, options.toBundle())

    }

    fun notifySearch(query: String) {
        if (viewModel.favorite) {
            viewModel.firstTime = true
        }
        viewModel.adapter.clearItems()
        viewModel.searchByTitle(query)
    }

}