package com.codeasy.movieapp.ui.movie.detail

import android.graphics.Bitmap
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.codeasy.movieapp.BuildConfig
import com.codeasy.movieapp.R
import com.codeasy.movieapp.data.models.Movie
import com.codeasy.movieapp.databinding.ActivityMovieDetailBinding
import com.codeasy.movieapp.factory.AppViewModelFactory
import com.codeasy.movieapp.ui.movie.MovieFavoriteViewModel
import com.codeasy.movieapp.ui.movie.list.MovieListActivityViewModel
import com.codeasy.movieapp.ui.movie.list.fragments.ItemMovieViewModel
import dagger.android.AndroidInjection
import org.jetbrains.anko.backgroundDrawable
import javax.inject.Inject

class MovieDetailActivity : AppCompatActivity() {

    companion object {
        const val intent_movie = "movie"
    }

    @Inject
    lateinit var viewModelFactory: AppViewModelFactory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MovieListActivityViewModel::class.java)
    }

    private val movieFavoriteViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MovieFavoriteViewModel::class.java)
    }

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMovieDetailBinding>(
            this,
            R.layout.activity_movie_detail
        )
    }

    lateinit var gradientDrawable: GradientDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            supportPostponeEnterTransition()

            initUI()

            observeViewModel()
        }

    }

    private fun initUI() {

        val movie = getMovieFromIntent()
        setSupportActionBar(binding.toolbarMovieDetail.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = null
        binding.toolbarMovieDetail.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        if (movie != null) {
            binding.movie = ItemMovieViewModel(movie)
        }
        if (movie != null) {
            binding.toolbarMovieDetail.heartCheckBox.isChecked = movie.favorite
        }

        val requestOptions = RequestOptions()
        requestOptions.circleCrop()

        Glide.with(this)
            .asBitmap()
            .apply(requestOptions)
            .load(binding.movie?.posterPath())
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    isFirstResource: Boolean
                ): Boolean {
                    supportStartPostponedEnterTransition()
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    supportStartPostponedEnterTransition()
                    Palette.from(resource!!).generate {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            val dominantColor = it?.getDominantColor(resources.getColor(R.color.black, null))!!
                            val colors: IntArray = intArrayOf(resources.getColor(R.color.black, null), dominantColor)
                            gradientDrawable = GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, colors)
                            gradientDrawable.cornerRadius = 0f
                            binding.viewGradient.backgroundDrawable = gradientDrawable

                        } else {
                            it?.getDominantColor(resources.getColor(R.color.black))
                        }
                    }

                    return false
                }
            }).into(binding.circularImage)

        binding.toolbarMovieDetail.heartCheckBox.setOnCheckedChangeListener { _, isChecked ->

            if (movie != null) {
                movieFavoriteViewModel.changeFavorite(movie, isChecked)
            }
        }
    }

    private fun getMovieFromIntent(): Movie? {
        return intent.extras?.getParcelable(intent_movie)

    }

    private fun observeViewModel() {
//        viewModel.getFavCharacterById(viewModel.character.id).observe( this , Observer { response ->
//            binding.toolbarCharacterDetail.heartCheckBox.isChecked = response != null
//        })
    }
}