# Movie App

This application is for the blue coding test. To prove my abilities developing mobile application in Android with Kotlin. 
The app should allow the users to see a list of movies with the basic info. Should be able to
sort the list, perform search and see the details of every movie. In addition the user will be
able to mark movies as favorite and see all those favorite movies in separate list.

## Screenshots


Splash Screen              |  Home Screen              | Detail Screen
:-------------------------:|:-------------------------:|:-------------------------:
![screenshot_1540059566](https://gitlab.com/sydg18/movie-app/raw/develop/docs/splash.png)  |  ![screenshot_1540059798](https://gitlab.com/sydg18/movie-app/raw/develop/docs/home.png) |![screenshot_1540059643](https://gitlab.com/sydg18/movie-app/raw/develop/docs/detail.png)


## Gif


![](https://gitlab.com/sydg18/movie-app/raw/develop/docs/resumen.gif)



## Architecture

The architecture used is MVVM (Model View ViewModel) with a combination of recent Android Architecture Components released By Google.
![mvvm](https://gitlab.com/sydg18/movie-app/raw/develop/docs/mvvm-architecture.png)

* [Architecture Jetpack](https://developer.android.com/jetpack/arch/) - A collection of libraries that help you design robust, testable, and maintainable apps. Start with classes for managing your UI component lifecycle and handling data persistence.
  * [DataBinding](https://developer.android.com/topic/libraries/data-binding/) - Declaratively bind observable data to UI elements.
  * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - Build data objects that notify views when the underlying database changes.
  * [Room](https://developer.android.com/topic/libraries/architecture/room) - Access your app's SQLite database with in-app objects and compile-time checks.
  * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - Store UI-related data that isn't destroyed on app rotations. Easily schedule asynchronous tasks for optimal execution.
* [Dagger](https://dagger.dev) - A fast dependency injector for Java and Android.
* [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java.
* [Glide](https://square.github.io/retrofit/) - A fast and efficient open source media management and image loading framework for Android that wraps media decoding, memory and disk caching, and resource pooling into a simple and easy to use interface.

 
